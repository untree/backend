<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Untree - Reset Password</title>
</head>
<body>
    <img src="{{url('img/Logo.png')}}" width="250" alt="Logo">

    <h3> Reset Password </h3>
    <hr>
    Hi, {{$username}}
    <br>
    <br>
    We have received your request for a new password. Here is your new password:
    <br>
    <br>
    <table>
        <tr>
            <td>New Password</td>
            <td>:</td>
            <th>{{$password}}</th>
        </tr>
    </table>
    <br>
    Regards,
    <br>
    Untree Team
</body>
</html>
