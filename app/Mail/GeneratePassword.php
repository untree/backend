<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePassword extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $password;
    public $username;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $password, $username)
    {
        $this->email = $email;
        $this->password = $password;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.reset_password',[
            'email' => $this->email ,
            'password' => $this->password,
            'username' => $this->username,
            ])->subject('Reset Password');
    }
}
