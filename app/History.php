<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';
    protected $fillable = [
        'barbershop_id', 'hairstylist_id', 'user_id', 'customer_name', 'status'
    ];
}
