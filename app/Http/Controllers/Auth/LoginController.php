<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*
     *
     *
     */
    public function login(Request $request)
    {
        $request->validate([
            'username'         => 'required|string',
            'password'      => 'required|string',
        ]);

        $credentials = $request->only('username', 'password');
//        dd(Auth::attempt($credentials,false));
        if(!Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if($request->remember_me)
            $token->expireds_at = Carbon::now()->addWeeks(1);

        return response()->json([
            'access_token'      => $tokenResult->accessToken,
            'token_type'        => 'Bearer',
            'expires_at'        => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString(),
            'role'              => $request->user()->getRole(),
            'scope'             => ($request->user()->getScope() != NULL ? $request->user()->getScope() : ''),
        ]);
    }

    public function authenticate(Request $request){
        $request->validate([
            'username'      => 'required',
            'password'      => 'required|',
        ]);

        $credentials = $request->only('username', 'password');
        if(!Auth::attempt($credentials))
            return response()->json(['message' => 'Unauthorized'], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if($request->remember_me)
            $token->expireds_at = Carbon::now()->addWeeks(1);

        return response()->json([
            'access_token'      => $tokenResult->accessToken,
            'token_type'        => 'Bearer',
            'expires_at'        => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString(),
            'role'              => $request->user()->getRole(),
            'scope'             => ($request->user()->getScope() != NULL ? $request->user()->getScope() : ''),
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message'           => 'Successfully Logged out',
        ]);
    }

    public function username()
    {
        return 'username';
    }
}
