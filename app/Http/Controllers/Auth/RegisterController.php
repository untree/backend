<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Validator;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'first_name'        => 'required|min:3|max:50|string',
            'last_name'         => 'required|min:3|max:50|string',
            'username'          => 'required|min:3|max:50|string|unique:users',
            'phone'             => 'required',
            'email'             => 'required|min:3|max:50|email|unique:users',
            'password'          => 'required|min:6|max:16|'
        ]);
        if($validate->fails()) return response()->json(['message' => $validate->messages()],422);

        $user = new User;
        $user->first_name       = $request->get('first_name');
        $user->last_name        = $request->get('last_name');
        $user->username         = $request->get('username');
        $user->email            = $request->get('email');
        $user->phone            = '+62'.$request->get('phone');
        $user->password         = bcrypt($request->get('password'));
        $user->save();

        $role = new Role();
        $role->user_id          = $user->id;
        $role->role_name        = 'Customer';
        $role->save();

        return response()->json(['message' => 'Successfully created user!'], 200);
    }
}
