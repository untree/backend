<?php

namespace App\Http\Controllers;

use App\Queue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return response()->json(Queue::all(), 200);
    }

    // GET ORDER LIST FOR CUSTOMER
    // ===========================
    public function queueForCustomer($user_id)
    {
        $queues = DB::select(
            DB::raw("
            SELECT
                q.id, q.barbershop_id, q.user_id, q.hairstylist_id, q.created_at, q.queue,
                h.hairstylist_name, b.barbershop_name, b.image_barbershop
            FROM
                queues q
            RIGHT JOIN hair_stylists h ON q.hairstylist_id = h.hairstylist_id
            RIGHT JOIN barbershops b ON q.barbershop_id = b.barbershop_id
            WHERE q.id IS NOT NULL AND q.user_id=".$user_id."
            ORDER BY q.created_at DESC")
        );
        if($queues) return response()->json($queues, 200);
        return response()->json('no-content', 204);
    }

    // GET ORDER LIST FOR BARBERSHOP
    // ==============================
    public function queueForBarbershop($barberId)
    {
        $queues = DB::select(
            DB::raw("
            SELECT
                q.*, concat(u.first_name, ' ', u.last_name) as full_name,
                h.hairstylist_name
            FROM queues q
            LEFT JOIN hair_stylists h ON h.hairstylist_id = q.hairstylist_id
            LEFT JOIN users u ON u.id = q.user_id
            WHERE barbershop_id=".$barberId."
            ORDER BY q.queue ASC")
        );
        if($queues) return response()->json($queues, 200);
        return response()->json('no-content', 204);
    }

    // GET ORDER DETAILS
    // ==================
    public function queueDetails($id)
    {
        $queues = DB::select(
            DB::raw("
            SELECT
                q.id, q.barbershop_id, q.hairstylist_id, q.user_id, q.created_at, q.queue,
                CONCAT(u.first_name, ' ', u.last_name) as full_name, u.phone,
                h.hairstylist_name, b.barbershop_name, b.address, b.phoneNumber, b.price
            FROM
                queues q
            RIGHT JOIN users u ON q.user_id = u.id
            RIGHT JOIN hair_stylists h ON q.hairstylist_id = h.hairstylist_id
            RIGHT JOIN barbershops b ON q.barbershop_id = b.barbershop_id
            WHERE q.id=".$id)
        );
        if($queues) return response()->json($queues[0], 200);
        return response()->json('Order Not Found', 404);
    }

    // GET MANUAL CUSTOMER ORDER DETAILS
    // =================================
    public function queueCustomerDetails($id)
    {
        $queues = DB::select(
            DB::raw("
            SELECT
                q.id, q.barbershop_id, q.hairstylist_id, q.user_id, q.customer_name, q.created_at, q.queue,
                h.hairstylist_name, b.barbershop_name, b.address, b.phoneNumber, b.price
            FROM
                queues q
            RIGHT JOIN hair_stylists h ON q.hairstylist_id = h.hairstylist_id
            RIGHT JOIN barbershops b ON q.barbershop_id = b.barbershop_id
            WHERE q.id=".$id)
        );
        if($queues) return response()->json($queues[0], 200);
        return response()->json('Order Not Found', 404);
    }

    // BOOK THE ORDER
    // ================
    public function booking(Request $request)
    {
        // Get QueueNumber
        $hairstylistId = Queue::where('hairstylist_id', $request->get('hairstylist_id'))->orderBy('created_at', 'desc')->first();
        if(!$hairstylistId) $queueCount = 0;
        else $queueCount = $hairstylistId->queue + 1;

        $request->validate([
            'barbershop_id' => 'required',
            'hairstylist_id'=> 'required',
            'user_id'       => 'required'
        ]);

        $queue = Queue::create([
            'barbershop_id' => $request->get('barbershop_id'),
            'hairstylist_id' => $request->get('hairstylist_id'),
            'user_id' => $request->get('user_id'),
            'queue' => $queueCount
        ]);

        return response()->json($queue->id, 201);
    }

    // ADD CUSTOMER MANUALLY
    // Add customer that come to the barbershop that doesn't use application
    // =====================================================================
    public function addCustomer(Request $request)
    {
        // Get QueueNumber
        $hairstylistId = Queue::where('hairstylist_id', $request->get('hairstylist_id'))->orderBy('created_at', 'desc')->first();
        if(!$hairstylistId) $queueCount = 0;
        else $queueCount = $hairstylistId->queue + 1;

        Queue::create([
            'barbershop_id' => $request->get('barbershop_id'),
            'hairstylist_id'=> $request->get('hairstylist_id'),
            'user_id' => 'GST',
            'customer_name'=> $request->get('customer_name'),
            'queue'=> $queueCount
        ]);
        return response()->json('Success', 201);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Queue  $queue
     * @return \Illuminate\Http\Response
     */
    public function show(Queue $queue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Queue  $queue
     * @return \Illuminate\Http\Response
     */
    public function edit(Queue $queue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Queue  $queue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Queue $queue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Queue  $queue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Queue $queue)
    {
        //
    }
}
