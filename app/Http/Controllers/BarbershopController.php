<?php

namespace App\Http\Controllers;

use App\Barbershop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarbershopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // GET ALL BARBERSHOP
    // ===============
    public function index()
    {
        return response()->json(Barbershop::all(),200);
    }

    // FILTERED BARBERSHOP LIST WITH HAIRSTYLIST
    // Barbershop that doesn't have a hairstylist will not appear
    public function barbershopFilter()
    {
        $barbershop = DB::select(
            DB::raw("
            SELECT * FROM barbershops b
            LEFT JOIN barbershop_hairstylists h ON b.barbershop_id = h.barbershop_id
            GROUP BY b.barbershop_id
            HAVING h.hairstylist_id")
        );
        if($barbershop) return response()->json($barbershop, 200);
        return response()->json('no-content', 204);
    }

    // GET ONE BARBERSHOP
    // ======================
    public function barbershop($id)
    {
        $barbershop = Barbershop::find($id);
        if($barbershop) return response()->json($barbershop, 200);
        abort(404, 'Barbershop not found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barbershop  $barbershop
     * @return \Illuminate\Http\Response
     */
    public function show(Barbershop $barbershop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barbershop  $barbershop
     * @return \Illuminate\Http\Response
     */
    public function edit(Barbershop $barbershop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barbershop  $barbershop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barbershop $barbershop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barbershop  $barbershop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barbershop $barbershop)
    {
        //
    }
}
