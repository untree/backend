<?php

namespace App\Http\Controllers;

use App\HairStylist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HairStylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //  GET ALL HAIRSTYLISTS
    // =======================
    public function index()
    {
        return response()->json(HairStylist::all(),200);
    }

    // GET ALL HAIRSTYLIST BASED ON CHOSEN BARBERSHOP
    // ===============================================
    public function hairstylists($barbershopId)
    {
        $hairStylist = DB::select(
            DB::raw("
            SELECT
                b.id, b.barbershop_id, b.hairstylist_id,
                h.hairstylist_name, count(q.queue) AS queue
            FROM
                barbershop_hairstylists b
            LEFT JOIN hair_stylists h ON b.hairstylist_id = h.hairstylist_id
            LEFT JOIN queues q ON b.hairstylist_id = q.hairstylist_id
            WHERE b.barbershop_id=".$barbershopId."
            GROUP BY b.hairstylist_id")
        );
        if($hairStylist) return response()->json($hairStylist, 200);
        return response()->json('no-content', 204);
    }

    // GET ONE HAIRSTYLIST DATA
    // ===========================
    public function getOneHairstylist($barbershopId, $hairstylistId) {
        $hairStylist = DB::select(
            DB::raw("
            SELECT
                b.id, b.barbershop_id, h.*,
                count(q.queue) as queue
            FROM barbershop_hairstylists b
            LEFT JOIN hair_stylists h ON b.hairstylist_id = h.hairstylist_id
            LEFT JOIN queues q ON b.hairstylist_id = q.hairstylist_id
            WHERE
                b.barbershop_id =".$barbershopId."
                AND
                h.hairstylist_id =".$hairstylistId
            )
        );
        if($hairStylist) return response()->json($hairStylist[0], 200);
        return response()->json('Hairstylist not found', 404);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // ADD NEW HAIRSTYLIST
    // =====================
    public function store(Request $request)
    {
        $hairstylist = new HairStylist();
        $hairstylist->hairstylist_name = $request['hairstylist_name'];
        $hairstylist->save();

        $list = DB::table('barbershop_hairstylists')->insert([
            'barbershop_id' => $request['barbershop_id'],
            'hairstylist_id' => $hairstylist->hairstylist_id
        ]);

        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HairStylist  $hairStylist
     * @return \Illuminate\Http\Response
     */
    public function show(HairStylist $hairStylist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HairStylist  $hairStylist
     * @return \Illuminate\Http\Response
     */
    public function edit(HairStylist $hairStylist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HairStylist  $hairStylist
     * @return \Illuminate\Http\Response
     */
    // EDIT HAIRSTYLIST
    // ===================
    public function update(Request $request, $id)
    {
        $hairstylist = HairStylist::find($id);
        $hairstylist->hairstylist_name = $request->get('hairstylist_name');
        $hairstylist->save();

        return response()->json('success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HairStylist  $hairStylist
     * @return \Illuminate\Http\Response
     */
    // DELETE HAIRSTYLIST
    // =====================
    public function destroy(Request $request, $id)
    {
        $hairstylist = HairStylist::where('hairstylist_id', $request['hairstylist_id'])->delete();
        if(!$hairstylist) return response()->json('Hairstylist not found', 422);
        return response()->json('success', 201);
    }
}
