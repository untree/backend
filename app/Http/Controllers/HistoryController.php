<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\History;
use App\Queue;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // GET CUSTOMER HISTORY
    // ====================
    public function getCustomerHistory($user_id)
    {
        $history = DB::select(
            DB::raw("
            SELECT h.*,  b.barbershop_name, hs.hairstylist_name
            FROM history h
            LEFT JOIN barbershops b ON h.barbershop_id = b.barbershop_id
            LEFT JOIN hair_stylists hs ON h.hairstylist_id = hs.hairstylist_id
            WHERE user_id=".$user_id."
            ORDER BY h.created_at DESC")
        );
        if(!$history) return response()->json('no-content', 204);
        return response()->json($history, 200);
    }

    // GET BARBERSHOP HISTORY
    // ======================
    public function getBarbershopHistory($barbershop_id)
    {
        $history = DB::select(
            DB::raw("
            SELECT h.*, CONCAT(u.first_name, ' ', u.last_name) AS full_name, hs.hairstylist_name
            FROM history h
            LEFT JOIN users u ON h.user_id = u.id
            LEFT JOIN hair_stylists hs ON h.hairstylist_id = hs.hairstylist_id
            WHERE h.barbershop_id=".$barbershop_id."
            ORDER BY h.created_at DESC"
            )
        );
        if(!$history) return response()->json('no-content', 204);
        return response()->json($history, 200);
    }

    // GET ONE HISTORY DETAILS
    // ========================
    public function getHistoryDetails($id) {
        $history = DB::select(
            DB::raw("
            SELECT
                hi.*, CONCAT(u.first_name, ' ', u.last_name) as full_name, u.username, u.email,
                h.hairstylist_name, b.barbershop_name, b.address, b.phoneNumber, b.price, b.image_barbershop
            FROM
                history hi
            RIGHT JOIN users u ON hi.user_id = u.id
            RIGHT JOIN hair_stylists h ON hi.hairstylist_id = h.hairstylist_id
            RIGHT JOIN barbershops b ON hi.barbershop_id = b.barbershop_id
            WHERE hi.id=".$id)
        );
        if(!$history) return response()->json('no-content', 204);
        return response()->json($history[0], 200);
    }

    // GET ONE HISTORY DETAILS
    // ========================
    public function getHistoryCustomerDetails($id) {
        $history = DB::select(
            DB::raw("
            SELECT
                hi.*, h.hairstylist_name, b.barbershop_name,
                b.address, b.phoneNumber, b.price, b.image_barbershop
            FROM
                history hi
            RIGHT JOIN hair_stylists h ON hi.hairstylist_id = h.hairstylist_id
            RIGHT JOIN barbershops b ON hi.barbershop_id = b.barbershop_id
            WHERE hi.id=".$id)
        );
        if(!$history) return response()->json('no-content', 204);
        return response()->json($history[0], 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * CLEAR QUEUE AFTER TRANSACTION
     * STORE TRANSACTION TO HISTORY DATABASE
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // CREATE HISTORY AFTER ORDER HAS BEEN COMPLETED OR CANCELED
    // ==========================================================
    public function store(Request $request)
    {
        // Decrement for Queue
        $queueData = Queue::where('id', $request['id'])->first()->queue;
        $queueCount = Queue::where('hairstylist_id', $request['hairstylist_id'])->pluck('queue');
        $i = 0;
        while ($i < count($queueCount)) {
            if ($queueCount[$i] > $queueData) {
                $queueCount[$i] = $queueCount[$i] - 1;
            }
            $i++;
        }
        // Update queue number
        $idArray = Queue::where('hairstylist_id', $request['hairstylist_id'])->pluck('id');
        for($i=0; $i < count($idArray); $i++) {
            Queue::where('id', $idArray[$i])->update(['queue'=> $queueCount[$i]]);
        }

        // Get data to stored to history table
        $id = Queue::where('id', $request['id'])->first()->id;
        $created_at = Queue::where('id', $request['id'])->first()->created_at;

        // DELETE QUEUE AFTER TRANSACTION
        $queue = Queue::where('id', $request['id'])->delete();
        if(!$queue) return response()->json('queue not found', 422);

        // Store TO HISTORY DATABASE
        $history = new History();
        $history->id = $id;
        $history->barbershop_id = $request['barbershop_id'];
        $history->hairstylist_id = $request['hairstylist_id'];
        $history->user_id = $request['user_id'];
        $history->customer_name = $request['customer_name'];
        $history->status = $request['status'];
        $history->created_at = $created_at;
        $history->save();
        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * Clear HISTORY BY USER ACCESS
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $history = History::where('user_id', $id)->delete();
        if(!$history) return response()->json('History Not found', 422);
        return response()->json('All History has been deleted', 201);
    }
}
