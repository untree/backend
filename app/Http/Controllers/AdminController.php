<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barbershop;
use App\HairStylist;
use App\Role;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    public function __constructor() {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    // SET ROLE FOR BARBERSHOP
    public function changeRole($user_id, $barbershop_id) {
        $role = Role::where('user_id', $user_id)->first();
        if(!$role) return response()->json('User not found', 204);

        $oldRole = Role::where('barbershop_id', $barbershop_id)->first();
        if($oldRole && $oldRole->role_name == 'Barbershop') {
          $oldRole->role_name = 'Customer';
          $oldRole->barbershop_id = NULL;
          $oldRole->save();
        }
        $role->role_name = 'Barbershop';
        $role->barbershop_id = $barbershop_id;
        $role->save();
        return response()->json('User role has been changed', 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //  ADD BARBERSHOP
    // ==================
    public function store(Request $request)
    {
        $barber = new Barbershop();
        $barber->barbershop_name = $request['barbershop_name'];
        $barber->phoneNumber = '+62'.$request['phoneNumber'];
        $barber->address = $request['address'];
        $barber->price = $request['price'];
        $barber->image_barbershop = $request['barberImage'];
        $barber->save();
        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //  EDIT BARBERSHOP
    // ==================
    public function update(Request $request, $id)
    {
        $barber = Barbershop::find($id);
        $barber->barbershop_name = $request->get('barbershop_name');
        $barber->phoneNumber = '+62'.$request->get('phoneNumber');
        $barber->address = $request->get('address');
        $barber->price = $request->get('price');
        $barber->image_barbershop = $request->get('barberImage');
        $barber->save();

        return response()->json('success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //  DELETE BARBERSHOP
    // ========================
    public function destroy(Request $request)
    {
        // Delete Hairstylists
        $id = DB::table('barbershop_hairstylists')->select('hairstylist_id')->where('barbershop_id', $request['barbershop_id'])->pluck('hairstylist_id');
        $hairstylist = HairStylist::whereIn('hairstylist_id', $id)->delete();

        // Change User Role
        $role = Role::where('barbershop_id', $request['barbershop_id'])->first();
        // if(!$role) return response()->json('User not found', 204);
        if($role){
            $role->role_name = 'Customer';
            $role->barbershop_id = null;
            $role->save();
        }

        // Delete Barbershop
        $barbershop = Barbershop::where('barbershop_id', $request['barbershop_id'])->delete();
        if(!$barbershop && !$hairstylist) return response()->json('Barbershop not found', 422);
        return response()->json('success', 201);
    }
}
