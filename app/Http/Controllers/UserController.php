<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use DB;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    // GET USER DATA
    // ===================
    public function user(Request $request)
    {
        $user = $request->user();
        $user->role = $request->user()->getRole();
        $user->scope = $request->user()->getScope();
        return response()->json($user);
    }

    // EDIT USER ACCOUNT
    // =================
    public function update(Request $request, $id) {
        $validate = Validator::make($request->all(), [
            'first_name'    => 'required|min:3|max:50',
            'last_name'     => 'required|min:3|max:50',
            'email'         => 'required|min:3|max:50|email|unique:users,email,'.$id,
            'phone'         => 'required',
            'username'      => 'required|min:3|max:50|string|unique:users,username,'.$id
        ]);
        if($validate->fails()) return response()->json(['message' => $validate->messages()], 422);

        $user = User::find($id);
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->phone = '+62'.$request->get('phone');
        $user->username = $request->get('username');
        $user->save();

        return response()->json('success', 200);
    }

    // CHANGE PASSWORD
    // ================
    public function changePassword(Request $request, $id) {
        $validate = Validator::make($request->all(), [
            'password' => 'required|min:6|max:16|same:conf_password',
        ]);

        if($validate->fails()) return response()->json(['errors'=>$validate->errors()], 422);

        $old_password = User::find($id);

        // New Password and old password cannot be same
        if (Hash::check($request->password, $old_password->password)) {
            return response()->json(['errors' => ['new_password' => 'New Password should not be same as old password.']], 422);
        }

        // Check if Old Password is not the same
        if($old_password) {
            if(!Hash::check($request->get('old_password'), $old_password->password)) {
                return response()->json(['errors' => ['old_password' => 'Your old password is wrong.']], 422);
            }
        }
        $old_password->password = bcrypt($request->get('password'));
        $old_password->save();

        return response()->json('success', 201);
    }

    // GET ALL USERS WITH CUSTOMER ROLES
    // =================================
    public function getUsers($barbershop_id) {
        $users = DB::select(
            DB::raw("
            SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as full_name, r.role_name as role
            FROM users u
            LEFT JOIN roles r ON u.id = r.user_id
            WHERE r.role_name = 'Customer' || r.role_name = 'Barbershop' && r.barbershop_id=".$barbershop_id."
            ORDER BY full_name")
        );
        if(!$users) return response()->json('No users', 204);
        return response()->json($users, 200);
    }
}
