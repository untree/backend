<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HairStylist extends Model
{
    //
    protected $primaryKey = 'hairstylist_id';
    protected $fillable = [
        'hairstylist_name',
    ];
}
