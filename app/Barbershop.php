<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barbershop extends Model
{
    protected $primaryKey = 'barbershop_id';
    protected $fillable = [
        'berbershop_name', 'address', 'phoneNumber', 'hairtylist_id', 'image_barbershop'
    ];
}
