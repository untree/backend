<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'user_id', 'role_name', 'barbershop_id',
    ];

    public function user() {
        return $this->hasOne(App\User::class);
    }
}
