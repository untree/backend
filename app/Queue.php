<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    protected $table = 'queues';
    protected $fillable = [
        'barbershop_id', 'hairstylist_id', 'queue', 'user_id', 'customer_name'
    ];
}
