<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \App\User::create([
            'first_name'    => 'Admin',
            'last_name'     => 'Admin',
            'username'      => 'admin',
            'email'         => 'syarif.rivai@gmail.com',
            'password'      => bcrypt('asdasd')
        ]);

        \App\Barbershop::create([
            'barbershop_name'   => 'Uncle Do',
            'address'           => 'Jalan Rawa Belong No. 53 Palmerah 1, RT.1/RW.9, Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11540',
            'phoneNumber'       => '081361999222',
            'image_barbershop'  => null
        ]);

        \App\HairStylist::create([
            'hairstylist_name'  => 'Tomo'
        ]);

        \App\Role::create([
            'user_id'           => 1,
            'role_name'         => 'Admin',
            'barbershop_id'     => NULL
        ]);

        DB::table('barbershop_hairstylists')->insert([
            'barbershop_id'     => 1,
            'hairstylist_id'    => 1,
        ]);
    }
}
