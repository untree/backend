<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BarbershopHairstylists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barbershop_hairstylists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('barbershop_id');
            $table->unsignedBigInteger('hairstylist_id');
            $table->timestamps();

            $table->foreign('hairstylist_id')
            ->references('hairstylist_id')->on('hair_stylists')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('barbershop_id')
            ->references('barbershop_id')->on('barbershops')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barbershop_hairstylists');
    }
}
