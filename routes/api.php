<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function() {

    Route::post('login', 'Auth\LoginController@authenticate')->name('login');
    Route::post('register', 'Auth\RegisterController@register')->name('register');

    Route::group(['middleware' => 'auth:api'], function() {
       Route::get('logout',     'Auth\LoginController@logout')->name('logout');
       Route::get('user',       'UserController@user');
       Route::get('role',       'RoleController@index');
    });

    // ++++++++++++++++++ Forgot Password ++++++++++++++
    Route::post('forgot/password/{email}',      'Auth\ForgotPasswordController@index');

    /* +++++++++++ Barbershop ++++++++++++
     * */
    Route::get('barbershop',                   'BarbershopController@index');
    Route::get('barbershop/filter',            'BarbershopController@barbershopFilter');
    Route::get('barbershop/{id}',              'BarbershopController@barbershop');

    /* +++++++++++ Hairstylist +++++++++++
     * */
    Route::get('hairstylist',                               'HairStylistController@index');
    Route::get('hairstylist/{id}',                          'HairStylistController@hairstylists');
    Route::get('hairstylist/{barbershop}/{hairstylist}',    'HairStylistController@getOneHairstylist');
    Route::post('hairstylist/add',                          'HairStylistController@store');
    Route::post('hairstylist/edit/{id}',                    'HairStylistController@update');
    Route::post('hairstylist/delete/{id}',                  'HairStylistController@destroy');
    /* ++++++++++ Queue +++++++++++++++++
     * */
    Route::get('/queue',                        'QueueController@index');
    Route::post('/queue/store',                 'QueueController@booking');
    Route::get('/queue/user/{user_id}',         'QueueController@queueForCustomer');
    Route::get('/queue/barber/{barberId}',      'QueueController@queueForBarbershop');
    Route::get('/queue/{id}',                   'QueueController@queueDetails');
    Route::get('/queue/customer/{id}',          'QueueController@queueCustomerDetails');
    Route::post('/queue/customer/store',        'QueueController@addCustomer');
    /* ++++++++++ History +++++++++++++++++
     * */
    Route::get('/history/{user_id}',                    'HistoryController@getCustomerHistory');
    Route::get('/history/barbershop/{barberId}',        'HistoryController@getBarbershopHistory');
    Route::get('/history/details/{historyId}',          'HistoryController@getHistoryDetails');
    Route::get('/history/details/customer/{historyId}', 'HistoryController@getHistoryCustomerDetails');
    Route::post('/history/store',                       'HistoryController@store');
    /* ++++++++++ User +++++++++++++++++
     * */
    Route::post('/edit/user/{id}',               'UserController@update');
    Route::post('/change/password/{id}',         'UserController@changePassword');
    Route::get('/get/users/{barbershop_id}',     'UserController@getUsers');
    /* ++++++++++ Admin +++++++++++++++++
     * */
    Route::post('admin/barbershop/add',                         'AdminController@store');
    Route::post('admin/barbershop/update/{id}',                 'AdminController@update');
    Route::post('admin/change/role/{user_id}/{barbershop_id}',  'AdminController@changeRole');
    Route::post('admin/barbershop/delete/{barbershop_id}',      'AdminController@destroy');
});
